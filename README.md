Programozás szakkör, Tahitótfalu, 2017 tavasz (12 alkalom x 3 óra)
==================================================================

> Utoljára módosítva: (2017-May-20_162854)

A szakkör bevezetés a számítógép-programozás világába, korszerű eszközökkel, interaktívan, játékosan, sok gyakorlással, önálló programozással és sikerélménnyel. 

Leginkább a Python nyelven keresztül ismerkedünk majd az imperatív programozás fogalmaival, de más nyelvekre/paradigmákra is kitekintéssel: érintjük az objektum-orientált és a funkcionális programozást is, a Python mellett megjelenik a C, Java, Haskell nyelv is. 

Csiszoljuk a problémamegoldó képességünket, gyakoroljuk az algoritmikus gondolkodást, az absztrakciót, az analitikus szemléletet. 

A modern programozó eszköztárának áttekintése, esetleg egy géptermi látogatás zárhatja a szemesztert, ha lesz érdeklődés és belefér az időnkbe. 

Mélyebb algoritmusok, bonyolultabb adatszerkezetek, egyéni és csapatversenyre felkészítés most még nem célunk. 

Lassú, alapozó munkát szeretnénk végezni.

Hirdetések
==========

1. Május 27 nem lesz jó a Faluháznak, valahova máshova kell mennünk. Valószínű, ekkor lesz a géptermi látogatás Budapesten, ezért nem lesz probléma.
2. Március 18.-án kora reggeltől 8.45-ig szemorvosi vizsgálat lesz az előtérben. Vagy 8.45 után kellene bejönni, vagy nagyon diszkréten, a rendelést nem zavarva.
3. Április 15. alkalom a korábban megbeszéltek szerint elmarad, következő szakkör április 22.
4. Felkerültek a táblafotók. A véletlenül túl korán letörölt Boole-algebra krétás táblarajzot a fehértáblán újrarajzoltam és feltettem azt is.
5. Felkerültek a letisztázott, megkommentezett példakódok is.
6. A május 27.-ei utolsónak tervezett alkalom sajnos elmarad. Az utolsó szakköri foglalkozás május 20.-án lesz.
7. ...

# Tematika:
## 1. hét   (március 4)
* Telepítés, letöltés. Python környezetek, dokumentáció, ajánlott irodalom.
* A Python nyelvről. Python 2.x, 3.x, különbségek. Motiváció, TIOBE index: miért pont Python?
* Python shell és IDLE IDE. Pycharm. Összesen 5 környezet. Elindítani, kilépni. 
* IDLE képernyő elemek. IDLE konfigurációs beállítások.
* Hello world program. A print utasítás/függvény.
* Többsoros program, több print egymás után. Egy sorban ;-vel elválasztva, vagy több sorban Enter-rel elválasztva. A szekvencia fogalma. Utasítások sorrendjének fontossága.
* Komment, dokumentációs komment. Programrészt kivenni/visszatenni hatékonyan. IDLE-ben Format->Comment out region/Uncomment region.
* Program mentése IDLE editorból .py forrásfile-ba. Python forrásfile-ok futtatása konzolból, és IDLE-be beolvasva.
* Python konzol, mint kalkulátor. Az _ változó mint részeredmény akkumulátor.
* Számábrázolási, kerekítési probléma: 0.1 + 0.1 + 0.1 - 0.3 nem nulla. Miért? (Tárgyalása a jövő héten.)
* Irodalom: 
    * [PFK] 1. fejezet
    * [Swin] 2. fejezet
    * [Tut] 1. - 3. fejezet
    * [Down] 1. fejezet
    * [Vord] 1. és 3. fejezet

## 2. hét   (március 11)
* Számábrázolási, kerekítési probléma: múlt heti példára visszatérni.
* Számrendszer fogalma. Nevezetes számrendszerek: 2, 8, 10, 16-os.
* Python konzolban kipróbálni a számrendszerek közötti konverziókat: 0b, 0o, 0x prefixek.
* A múlt heti 0.1 számábrźolási probléma oka.
* A változó fogalma. Változók típusa. Nevezetes Python típusok: int, float, str.
* Műveletek változókon. Értékadás, többszörös értékadás. Változó értékének felülírása, mint temporális kifejezés.
* Az input() függvény. Hello world programot továbbírni: legyen benne input() és változó. Köszöntse a felhasználót a nevén, amit előtte bekért.
* Logo. Seymour Papert. Python turtle modul alapok. Rajzolás a turtle modullal. Turtle modul függvényei.
* Irodalom:
    * [PFK] 2. és 4. fejezet
    * [Swin] 5. fejezet
    * [Tut] 15. fejezet
    * [Down] 2. fejezet

## 3. hét   (március 18)
* Neumann architektúra. Neumann-elvek.
* Neumann architektúrájú számítógép felépítése.
* Tipikus frekvenciák és ciklusidők a számítógépben. A ceruzahegyezős példa.
* Az elágazás fogalma. Feltételes utasítás (1 ágú), kizáró vagy alternatív utasítások (2 ágú.)
* Python szintaxis: if, if...else. 
* Pass utasítás az üres ágaknak, be nem fejezett összetett utasításoknak.
* Gyakorlat: önálló program, bekérni egy számot, eldönteni negatív vagy nemnegatív.
* Többágú elágazások. Közlekedési lámpa példa. Egyre mélyebbre tabulálás probléma: if...else: if ... 
* Az elif kulcsszó, mint szintaktikus cukorka.
* Közlekedési lámpa átírása if...elif...else alakra.
* Gyakorlat: abszolút érték programot megírni, kezelni a >0, =0, <0 esetet is, elif-fel.
* Logikai kifejezések. True, False konstansok, bool(0), bool(1) kipróbálása konzolban.
* Összehasonlító operátorok: <, >, ==, <=, >=, !=. 
* Értékadás (=) és egyenlőségvizsgálat (==) különbözősége.
* Irodalom:
    * [PFK] 5. fejezet
    * [Swin] 3. fejezet
    * [Tut] 4. fejezet
    * [Down] 5. fejezet

## 4. hét   (március 25)
* Boole-algebra.
* Igazságtáblázatok: és, vagy, kizáró vagy műveletek, és logikai tagadás.
* Állítás. Állítás igazsághalmaza. Venn-diagramos ábrázolás.
* Önálló gyakorlás Python-ban: állítások if...elsif...else:-ben.
* Ismétlődő programutasítások: a ciklus fogalma. 
* Elöltesztelő ciklus. Python szintaxis: while ciklus. Példa. Önálló gyakorlás
* Motiváció a ciklusokhoz: sokszögek rajzolása teknőcgrafikával. Négyzet és háromszög rajzolása oldalanként.
* Rajzolásban a minta felismerése, egy adott síkidom kiemelése while ciklusba, de még mindig külön ciklus síkidomonként.
* Újabb minta felismerése a sokszögek között. Ciklusba ágyazott ciklus fogalma. Szabályos 3..18-szög megrajzolása egyetlen, ciklusba ágyazott ciklussal.
* Python szintaxis: for ciklus. Összevetése a while ciklussal: mikor melyiket cékszerű használni.
* Ciklus manipulálása a ciklusmagon belül: break, continue, else:.
* Irodalom:
    * [PFK] 6. és 11. fejezet
    * [Swin] 4. fejezet
    * [Down] 7. fejezet
    * [Vord] 4. fejezet

## 5. hét   (április 1)
* Ismétlés, gondolkodtató feladatok.
* Változókkal, értékadással kapcsolatban: két változó értékének felcserélése.
* Elágazásokkal kapcsolatban: FizzBuzz (RókaBéka) oszthatósági játék.
* Ciklusokkal kapcsolatban: hátultesztelő ciklus megvalósítása Pythion-ban (vagy más nyelven)
* Tipikus játékprogram hurok: látszólag végtelen ciklus egy feltételes break-kel, ami a játék végét kezeli...
* A függvény fogalma, függvények írásának és meghívásának szintaxisa Python-ban.
* Gyakorlat: a nagyobb() függvény megírása önállóan
* Függvényparaméter. Paraméter nélküli függvények. Paraméter átadása. Paraméter fogadása lokális változóban.
* A névtér fogalmának első bevezetése. Függvény definíciója meg kell előzze a használatát.
* Függvények kompozíciója.
* A lista fogalma. Lista szintaxisa Python-ban. Üres lista: [].
* Lista bejárása elemenként, számlálás nélkül, for ciklussal. 
* Az enumerate() függvény. Lista bejárása for ciklussal és számlálással.
* Műveletek, függvények listákon: lista hossza, string szavak listájára tördelése, szavak listájának összefűzése strngbe. Lista helyben rendezése.
* Lista és változó hivatkozások, értékadások közötti különbség.
* Önálló munka: cenzor() függvény megírása: stringből adott mintának megfelelő szót kisípolni.
* Irodalom:
    * [PFK] 7. és 9. fejezet
    * [Swin] 6. és 7. fejezet
    * [Tut] 6. fejezet
    * [Down] 3. és 6. fejezet


## 6. hét   (április 8)
* Változók láthatósága és élettartama. A névtér fogalma.
* Global kulcsszó. Paraméter átvétele a függvényben lokális változóként.
* Globális változó elfedése lokális változóval.
* Kör, ellipszis és tojás rajzolása teknőcgrafikával. 
    * (A körig sikerült csak eljutni.)
    * (Módszer: [x-r,x+r] intervallumon Pitagoasz-tétellel kettesével rajzolni a pontokat.)
* Refaktorálás. Ismétlődő programrészek felismerése, kiemelése külön függvénybe.
* Irodalom:
    * [Down] 4. fejezet

## (április 15. SZÜNET)

## 7. hét   (április 22)
* Adatszerkezetek: 
    * string (szöveg)
    * list (lista)
    * tuple (rendezett n-es véges lista)
    * Szekvencia adattípusok: string, list, tuple, range. Mert rákövetkezővel felsorolhatóak.
    * dictionary (szótár)
    * set (halmaz)
    * Ezek fogalmi bevezetése, összevetése, melyik mire való.
    * Szintaxis: melyiket hogyan jelöljük: 
        * "string", 'string', """multiline string""", ''''multiline string''''
        * lista ::= [e1,e2, ..., en]
        * tuple ::= (c1,c2, ..., cn)
        * dict ::= {k1:v1, k2:v2, ... , kn:vn}
        * set ::= {e1,e2,...,en}
* String bővebben
    * konkatenáció
    * ismétlés (string szorzata)
    * string hossza
    * indexelés
    * kivágás, szeletelés
    * string bejárása for .. in iterátorral
    * tartalmazás vizsgálata: in, not in
    * összehasonlítás
    * változó behelyettesítése string-be
    * stringek típuskényszerítése: int(), float()
    * string metódusok (rengeteg, ld. [Tut] 4.7.1)
* Lista bővebben
    * elemek elérése, indexelés
    * lista módosítása
    * szeletelés
    * számok listája range()-el
    * lista hossza
    * lista bejárása for .. in iterátorral
    * elemek lista végére fűzése
    * tartalmazás vizsgálata: in, not in
    * lista másolása
    * lista metódusok (rengeteg, ld. [Tut] 5.1)
    * hatékony beszúrás a lista elejére
    * többdimenziós listák: listák egymásba ágyazása
* Irodalom:
    * [PFK] 3. fejezet
    * [Swin] 10. fejezet
    * [Tut] 3.1.2, 3.1.3 rész, 4.6, 4.7, 4.10 részek, 5. fejezet
    * [Down] 8., 10., 11., 12. fejezet
    * Gyakorlat [Down] 9. és 13. fejezet 

## 8. hét   (április 29)
* Közös programozás: számkitalálós játék
* Adatszerkezetek, folytatás a múlt hétről.
* Tuple (rendezett n-es véges lista) bővebben
    * tuple létrehozása
    * 1-elemű tuple
    * üres tuple
    * tuple() fgv. használata szekvenciával
    * szeletelés
    * két tuple összehasonlítása
    * string tördelése tuple-be
    * több visszatérési érték fgv.ből: tuple!
    * szekvenciák cippzárazása tuple-be
    * cippzár bejárása for ciklussal
    * cippzár tuple-listává konvertálása
    * egyszerre több szekvencia iterálása
    * enumerate() fgv> range()-ből (index, érték) tuple
* Dictionary (szótár) bővebben
    * szótár létrehozása
    * szótár kulcsainak listája, rendezett listája
    * kulcs tartalmazás ellenőrzése szótárban
    * szótárhoz hozzáírás
    * szótárban felülírás
    * törlés szótárból
    * szótár értékeinek listája
    * kulcshoz tartozó érték olvasása szótárból
* Set (halmaz) bővebben
    * halmaz létrehozása
    * üres halmaz létrehozása
    * halmazhoz tartozás ellenőrzése
    * halmazműveletek Python-ban
        * unió
        * metszet
        * különbség
        * szimmetrikus differencia
* Irodalom (Adatszerkezetek):
    * [PFK] 3. fejezet
    * [Swin] 10. fejezet
    * [Tut] 3.1.2, 3.1.3 rész, 4.6, 4.7, 4.10 részek, 5. fejezet
    * [Down] 8., 10., 11., 12. fejezet
    * Gyakorlat [Down] 9. és 13. fejezet 

## 9. hét   (május 6)
* Kivételkezelés. Futás közbeni hibák.
* Számkitalálós játék kiegészítése kivételkezeléssel.
* Torpedó játék vázlata
* Irodalom:
    * [Tut] 8. fejezet
    * [Swin] 9. fejezet

## 10. hét  (május 13)
* Torpedó játék befejezése
* Számítógép felépítése, alkatrészek: szemléltetés bontott alaplap segítségével
* Filekezelés, I/O.
* Adatok mentése fileba, visszatöltése fileból. JSON. Típuskonverzió.
* Irodalom:
    * [Swin] 9. fejezet
    * [Tut] 7. fejezet
    * [Down] 14. fejezet

## 11. hét  (május 20, SZAKKÖR VÉGE)
* Hasznos modulok, standard library túra, "battery included" filozófia
	* Parancs-sori paraméterek. A sys modul.
	* Web lekérdezés pythonból. Az urllib modul.
	* Dátum és idő manipuláció. A datetime modul.
	* Tesztelés. A doctest modul.
	* Egyéb hasznos modulok.
	* Irodalom:
    * [PFK] 10. fejezet
    * [Tut] 10. és 11. fejezet
* KöMaL feladatismertetés és a megoldás vázlata (I/421, 2017 február: https://www.komal.hu/verseny/feladat.cgi?a=honap&h=201702&t=inf&l=hu)
* Objektum-orientált programozási paradigma
	* Osztály fogalma és objektum fogalma. Példányosítás. A class és self kulcsszavak.
	* Leszármaztatás, öröklődés. Szülő és gyermek osztályok.
	* Objektumok inicializálása: az \_\_init\_\_ függvény
	* Példányváltozók. Attribútumok, property-k, getter/setter függvények.
	* Egységbe zárás.
	* Objektumok kompozíciója.
	* Polimorfizmus. Duck typing.
	* Absztrakt osztály és metódus
	* Irodalom (OO):
		*  [PFK] 8. fejezet
		*  [Swin] 11. és 12. fejezet
		*  [Tut] 9. fejezet
		*  [Down] 15.-18. fejezet
* Szakköri összefoglaló:
    * Miről volt szó?
    * Mi maradt ki?
    * Merre érdemes továbbmenni?
* Kérdezz-felelek

## 12. hét  (május 27) (SAJNOS ELMARAD)
* Külső helyszin? Budapest?
* Géptermi látogatás

## Lehetséges témák még, ha marad rá idő (TERVEZET)
* import this
* Többi játékprogram, feladatok az Irodalomból: [PFK] bounce, Mr. Stick Man, [Swin] ágyúpárbaj, ping
    * [PFK] 13. és 14., 15.-18. fejezet 
    * [Swin] 15. fejezet
* assert
* unit testing
* interpreter, compiler, VM+bytekód
* Hello world több nyelven: C, Java, Python
* Rekurzió, Haskell kitekintés, imperatív vs. funkcionális paradigma
* call stack, crash a stack felülírása miatt
* Matematikai jelölések és Python:
    * volt: elágazás, abszolút érték teljes diszkusszió
    * volt?: list comprehension
    * ciklushoz: PI értékét közelíteni
* KöMaL feladatmegoldás (I/421?)
* Irodalom:
    * [Down] 19. fejezet, Appendix B.
    * [SICP] 1.5.6 rész
    * [Swin] 1. fejezet

## Tematikából kimarad, noha az Irodalomban szerepel
* [PFK] 12. fejezet
* [Swin] 8., 13.-14., 16., 17., 18. fejezet
* [Tut] 12.-16. fejezet
* [Vord] 2. és 5. fejezet

Telepítési segédlet (Windows):
==============================
Windows felhasználók így tudják ellenőrizni, hogy 32 vagy 64 bites-e a Windows-uk. 
Ez az információ majd kelleni fog a telepítésekhez.
~~~~
wmic os get osarchitecture
~~~~
vagy (nem-adminisztrátoroknak):
~~~~
echo %PROCESSOR_ARCHITECTURE%
~~~~

Ami kelleni fog a szakkörben, **kéretik** feltelepíteni:
========================================================
- python 2.x: https://www.python.org/downloads/release/python-2713/
- python 3.x: https://www.python.org/downloads/release/python-360/
  
  > Igen, mindkét verzió kellhet, és egymástól függetlenül lehet őket telepíteni.
  
  > Pl. így, de a konkrét verzió nálad eltérhet:

  ~~~~
  $ python --version
  Python 2.7.6
  $ 
  $ python3 --version
  Python 3.4.3
  $ 
  ~~~~
  
- python 2.x **offline** dokumentáció: https://docs.python.org/2/download.html
- python 3.x **offline** dokumentáció: https://docs.python.org/3/download.html
  
  > Ezek azért kellenek, mert nem lesz wifi a szakkörben.

- IDLE, a Python IDE, mindkét Python verzióhoz 
  
  > Ha minden igaz, a fenti python csomagokkal feltelepül idle és idle3 is, de azért ellenőrizd:

  ~~~~
  $ idle  -c "import sys; print (sys.version); quit()"
  $ idle3 -c "import sys; print (sys.version); quit()"
  ~~~~

- JetBrains PyCharm-ból az **ingyenes** (Edu, Community Ed., tehát **nem** a Professional): https://www.jetbrains.com/pycharm-edu/download/

  > Legjobb lenne az EDU kiadást használni, abban van egy egész jó Python oktató is. Esetleg még a Community Edition ajánlott, ez szintén ingyenes.
  > A fizetős Professional-ra biztosan nem lesz szükségünk.

Jó ha van, de **nem muszáj** feltelepíteni:
===========================================
- git: https://git-scm.com/downloads
- Haskell: https://www.haskell.org/platform/
- Java SE JDK: http://www.oracle.com/technetwork/java/javase/downloads/index.html
- make, gcc, gdb: Linux-on már valószínű fent van, Windows-on cygwin unix-emulációba csomagolva: https://cygwin.com/install.html
- kedvenc programozó szövegszerkesztőd, ha már van (vim, emacs, atom, ...), python-ra idomítva (syntax highlight, stb)

Ajánlott irodalom, **érdemes** letölteni azt, ami ingyenes:
===========================================================
- [PFK]     Jason R. Briggs: Python for Kids, nem ingyenes: http://jasonrbriggs.com/python-for-kids/
- [SICP]    John Denero: SICP in Python: https://www.gitbook.com/book/wizardforcel/sicp-in-python/details
- [Swin]    Gérard Swinnen: Tanuljunk meg programozni Python nyelven: http://mek.oszk.hu/08400/08435/
- [Tut]     Python 2.x tutorial magyar fordítása (régi): http://pythontutorial.pergamen.hu/downloads/tut.pdf
- [Tut]     Python 3.x tutorial magyar fordítása (nincs teljesen kész): http://harp.pythonanywhere.com/python_doc/tutorial/
- [Down]    Allen Downey: Think Python. How to Think Like a Computer Scientist: http://greenteapress.com/thinkpython2/thinkpython2.pdf
- [Vord]    Carol Vorderman: Programozás gyerekeknek. HVG Könyvek, nem ingyenes: https://www.hvgkonyvek.hu/programozas-gyerekeknek


