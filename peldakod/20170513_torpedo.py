# 2017.05.06-13
# Torpedó játék, majdnem kész
# A számkitalálós játék alapján

# adatszerkezet emlékeztető

# Adatszerkezet: lista listában!
### többdimenziós listák: listák egymásba ágyazása
##linl = [ ["a", "b", "c"] , ["x","y","z"] ]
##print (type(linl), len(linl), linl)
##
### Példa többdimenziós listára. (Sakktábla: 2 dimenziós.)
##sor = [0] * 8
##sakktabla = [sor] * 8
##print ("Sakktábla (csupa nulla):" , sakktabla)
##
##sakktabla2 = []
##for i in range(8): sakktabla2.append([i]*8)
##print("Sakktábla (jobban olvasható):" , sakktabla2)
##
### Sakktábla pretty print
##for i  in range(len(sakktabla2)): print(sakktabla2[i])
### ======================================

# Sejtés: a torpedó jétékmező legyen listában lista, 2 dimenziós mint a sakktábla
# Sejtés: a játék menete teljesen hasonló a számkitalálóshoz.

from random import randint
from math import sqrt

jatekmezo = []
for sor in range(5):
    jatekmezo.append(["."]*5)

# első megközelítés
# print (jatekmezo)

# játékmező pretty print
# jó ez, csak a while ciklusba kellene tenni inkább
##for i in range(len(jatekmezo)): # tkp. range(5), csak "szebb"
##    print(jatekmezo[i])

hajo_sor        = randint(1,5)
hajo_oszlop     = randint(1,5)

print ("A hajóm: " , hajo_sor, hajo_oszlop)

torpedok_meg = 5

def tavolsag(hajo_sor , hajo_oszlop, tipp_sor, tipp_oszlop):
    """
    A lövés és a hajó távolsága
    Pitagorasz-tétel alkalmazása
    """
    return ( sqrt( (tipp_oszlop-hajo_oszlop)**2+(tipp_sor-hajo_sor)**2 ) )

while torpedok_meg > 0:
    # játékmező pretty print
    for i in range(len(jatekmezo)): # tkp. range(5), csak "szebb"
        # print(jatekmezo[i])
        # tipp:
        print(" ".join(jatekmezo[i]))

    print("Van még %d torpedód" % (torpedok_meg) )

    # Hiányzik: hibakezelés mindkettőre, mint a számkitalálósban
    tipp_sor        = int(input("Hova lősz? (sor): ")) 
    tipp_oszlop     = int(input("Hova lősz? (oszlop): "))

    print ( tipp_sor, tipp_oszlop )

    if (hajo_sor == tipp_sor and hajo_oszlop == tipp_oszlop):
        print("Talált!")
        break
    else:
        if ( jatekmezo[tipp_sor-1][tipp_oszlop-1] == "X"):
            print("Oda már lőttél!")
            # torpedok_meg = torpedok_meg + 1
        elif ():
            pass

        elteres = tavolsag(hajo_sor , hajo_oszlop, tipp_sor, tipp_oszlop)
        print("Majdnem, de a távolság: " , elteres )

    # ciklus legvégén adminisztrálunk
    jatekmezo[tipp_sor-1][tipp_oszlop-1] = "X"
    torpedok_meg = torpedok_meg - 1

print("Vége a játéknak...")
