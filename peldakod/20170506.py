# 2017.05.06
# Kivételkezelés: futás közbeni hibaállapotok

# Ez kell a kézzel összerakott saját kivételünkhöz
# https://docs.python.org/3/library/sys.html?highlight=sys.exc#sys.exc_info
import sys

def osztas(x, y):
    try:
        hanyados = x / y

        if y == 1:
            raise ValueError ("Minek osztani 1-el, úgyse változik semmi...")
        
    except ZeroDivisionError:
        print("Nullával osztás!")
##    except TypeError:
##        print ("Nem is szám paraméter!")
    except ValueError:
        print ("Fura számokat akarsz osztani, de azért megcsinálom..." )
        # https://docs.python.org/3/library/sys.html?highlight=sys.exc#sys.exc_info
        print (sys.exc_info()[1])
        return hanyados

    # nem ajánlott, csak ha tovább is dobod
    # az elkapott, akármilyen kivételt
    except:
        print ("Valami gáz van! Továbbdobom, csinálj vele amit akarsz...")
        raise

    # ez az alapeset, amikor nincs kivétel,
    # és minden a tervek szerint alakul
    else:
        print("A hányados a függvényben: ", hanyados)
        return hanyados

    # ide mindig befut, akár dobott kivételt, akár nem
    # itt lehet a lefoglalt erőforrásokat felszabadítani
    finally:
        print("A finally ágban vagyok...")

# Főprogram: teszteli az osztas() függvényt

# alapeset: semmilyen kivételt nem dob
print("=" * 20)
print ("6/2 eredménye: ")
print ("Osztás eredménye: " , osztas(6, 2))
print("=" * 20 + "\n")

# nullával osztás gyári kivételt dob
print("=" * 20)
print ("2/0 eredménye: ")
print ("Osztás eredménye: " , osztas(2, 0))
print("=" * 20 + "\n")

# 1-el osztás kézzel fabrikált kivételt dob
print("=" * 20)
print ("2/1 eredménye: ")
print ("Osztás eredménye: " , osztas(2, 1))
print("=" * 20 + "\n")

# TypeError-t kezdetben nem kezeltük le
# => el is esett rajta, de felvettük új except: ágként
print("=" * 20)
print("'2' / '1' eredménye: ")
print ("Osztás eredménye: " , osztas("2", "1") )
print("=" * 20 + "\n")

