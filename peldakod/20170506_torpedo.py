# 2017.05.06
# Torpedó játék vázlata
# A számkitalálós játék alapján

# adatszerkezet emlékeztető

# Adatszerkezet: lista listában!
### többdimenziós listák: listák egymásba ágyazása
##linl = [ ["a", "b", "c"] , ["x","y","z"] ]
##print (type(linl), len(linl), linl)
##
### Példa többdimenziós listára. (Sakktábla: 2 dimenziós.)
##sor = [0] * 8
##sakktabla = [sor] * 8
##print ("Sakktábla (csupa nulla):" , sakktabla)
##
##sakktabla2 = []
##for i in range(8): sakktabla2.append([i]*8)
##print("Sakktábla (jobban olvasható):" , sakktabla2)
##
### Sakktábla pretty print
##for i  in range(len(sakktabla2)): print(sakktabla2[i])
### ======================================

# Sejtés: a torpedó jétékmező legyen listában lista, 2 dimenziós mint a sakktábla
# Sejtés: a játék menete teljesen hasonló a számkitalálóshoz.

from random import randint

jatekmezo = []
for sor in range(5):
    jatekmezo.append(["."]*5)

# első megközelítés
# print (jatekmezo)

# játékmező pretty print
# jó ez, csak a while ciklusba kellene tenni inkább
##for i in range(len(jatekmezo)): # tkp. range(5), csak "szebb"
##    print(jatekmezo[i])

hajo_sor        = randint(1,5)
hajo_oszlop     = randint(1,5)

print (hajo_sor, hajo_oszlop)

torpedok_meg = 5

while torpedok_meg > 0:
    # játékmező pretty print
    for i in range(len(jatekmezo)): # tkp. range(5), csak "szebb"
        print(jatekmezo[i])
        # tipp:
        # print(" ".join(jatekmezo[i]))

    # Hiányzik: hibakezelés mindkettőre, mint a számkitalálósban
    tipp_sor        = int(input("Hova lősz? (sor): "))
    tipp_oszlop     = int(input("Hova lősz? (oszlop): "))

    print ( tipp_sor, tipp_oszlop )

    jatekmezo[tipp_sor-1][tipp_oszlop-1] = "X"
    
    torpedok_meg = torpedok_meg - 1

