"""
2017.05.20
"""

# ==========================================
# [Tut] 10.3 fejezet, https://docs.python.org/3/tutorial/stdlib.html#command-line-arguments
# NB. valami paraméterekkel hívni a programot
# python3 20170520_stdlib.py hello 42

import sys
print(sys.argv)

# Listával tér vissza
# 1. paraméter a python script neve, utána a parancssori paraméterek mint stringek
# Pl. a torpedó játéknak át lehet adni paraméterként, 
# mekkora legyen a játékmező és hány lövése lehessen a játékosnak.
# ==========================================

# ==========================================
# [Tut] 10.6 fejezet, https://docs.python.org/3/tutorial/stdlib.html#mathematics
# Nevezetes csomagok:
#   math
#   random
#   statistics
# ==========================================

# ==========================================
# [Tut] 10.7. Internet Access, https://docs.python.org/3/tutorial/stdlib.html#internet-access
# honlap feldolgozása

from urllib.request import urlopen
with urlopen('http://www.tahitotfalu.hu') as valasz:
    for sor in valasz:
        sor = sor.decode('utf-8')
        if ( 'pályázat' in sor.lower() or 'eperfesztivál' in sor.lower() ):
            print(sor)
# ==========================================

# ==========================================
# [Tut] 10.8. https://docs.python.org/3/tutorial/stdlib.html#dates-and-times
# https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior

from datetime import date
import locale

ma = date.today()
print(ma)

locale.setlocale(locale.LC_TIME, "en_GB.utf-8")
print(ma.strftime("angolszász: %m-%d-%y. %d %b %Y is a %A on the %dth day of %B.") )

locale.setlocale(locale.LC_TIME, "hu_HU.utf-8")
print(ma.strftime("magyar:     %Y.%m.%d, %Y %B (%b) %d %A napra esik %B hó %d. napján.") )

from datetime import timedelta
szulinap = date(2003, 5, 20)
kora = ma - szulinap
print ("Ma %d napos" % (kora.days))
print ("40 nappal ezelőtti dátum:" , date.today()-timedelta(days=40))
# ==========================================


# ==========================================
# [Tut] 10.11 https://docs.python.org/3/tutorial/stdlib.html#quality-control
# Tesztelés, nagyon hasznos!!!

# NB. -v-vel futtatni a script-et
# python3 20170520_stdlib.py -v

from math import sqrt
def tavolsag(hajo_sor , hajo_oszlop, tipp_sor, tipp_oszlop):
    """
    A lövés és a hajó távolsága
    Pitagorasz-tétel alkalmazása
    >>> print( tavolsag( 0,0,3,4 ) )
    5.0

    >>> print( tavolsag( 10,20,13,24 ) )
    5.0

    >>> print( tavolsag( 13,24,10,20 ) )
    5.0

    >>> print( tavolsag(0,0,0,0 ))
    0.0
    """
    # ez a jó visszatérési érték
    return ( sqrt( (tipp_oszlop-hajo_oszlop)**2+(tipp_sor-hajo_sor)**2 ) )

    # direkt elszúrni, megnézni, milyen az, amikor bukik a teszteken
    # return ( sqrt( (tipp_oszlop-hajo_oszlop)**2+(tipp_sor-hajo_sor)**2 ) + 1 )

import doctest
doctest.testmod()

# ==========================================
# [Tut] 11. fejezeten is végigmenni
# Különösen a 11.7, listakezelés
# és 11.8, decimal modul
# ==========================================

