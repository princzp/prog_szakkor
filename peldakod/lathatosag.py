g = 5

# def előtt nem lehet használni
# inc(42)

def inc(x):
    # global g #!!!
    g = 0
    g = g + 1
    x = x + 1
    print ("inc()-ben: " , g,x,a)

    # globális változó létrehozása lokális névtérben
    global g2
    g2 = "globális változó"

    return True # x # vagy True

# Hiba: x mostanra megszűnt
# print (x)

a = 42
a = inc(a)
print (g , a)

# !!! g2-t az inc() fgv. hozza létre!!!
print (g2)
