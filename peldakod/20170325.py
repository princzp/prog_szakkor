# 2017.03.25

"""
While ciklus
Elöltesztelő: lefuthat nullaszor, egyszer, többször vagy végtelenszer
"""

x = 1
while (x<6):
    print("hello", x)
    # FONTOS!!! A ciklusváltozót kézzel állítani!
    x = x + 1 # rövidebben: x+=1 

print("Ciklus vége.")
input("Program vége, nyomj Enter-t...")
    
"""
Motiváció a ciklusokhoz: sokszögek rajzolása
"""

from turtle import *
reset()

# rajzolás felgyorsítása
# delay(0)

# négyzet

forward(100)

left(90)
forward(100)

left(90)
forward(100)

left(90)
forward(100)

left(90)

# itt lehetne törölni is a vásznat
# reset()

# háromszög
forward(100)

left(120)
forward(100)

left(120)
forward(100)

left(120)

input("Program vége, nyomj Enter-t...")

"""
Szabályos 3,4 és 5-szög rajzolása.
A left()...forward() minta felismerése
Oldalak rajzolása ciklusba absztrahálva.
"""

reset()

# Háromszög
i=1
while i<=3:
    forward(100)
    left(360/3)
    i+=1

# Négyszög
i=1
while i<=4:
    forward(100)
    left(360/4)
    i+=1

# Ötszög
i=1
while i<=5:
    forward(100)
    left(360/5)
    i+=1

input("Program vége, nyomj Enter-t...")

"""
Szabályos sokszögek rajzolása
háromszögtől 18-szögig.
Ciklusba ágyazott ciklus.
"""
reset()

hanyszog=3

while (hanyszog<=18):
    oldal=1
    while (oldal<=hanyszog):
        forward(100)
        left(360/hanyszog)
        oldal += 1
    print("Szabályos" , hanyszog , "-szög kész.")
    hanyszog +=1

input("Program vége, nyomj Enter-t...")

"""
For ciklus szintaxisa,
összehasonlítás while ciklussal.
"""

i=1
while (i<=5):
    print("hello (while)" , i)
    i += 1

# igen, 6-ig!
for j in range(1,6):
    print("hello (for)", j)

    # j += 1 NEM KELL!!! 
    # a for ciklus automatikusan
    # növeli/csökkenti a ciklusváltozót

input("Program vége, nyomj Enter-t...")

"""
Ciklus manipulálása a ciklusmagon belül:
- break
- continue
- else
kulcsszavak használata
"""

i = 10
while (i<=15):
    # break: azonnal kilép a ciklusból
    # és az else:-be sem fut be
    # Próbáld ki!
    # if (i==13): break

    # continue: a ciklusmag elejére ugrik
    # de folytatja a while ciklust
    # a végén még az else:-be is belefut
    if (i==13):
        print(":(")
        i += 1 # FONTOS!!!
        continue
    print (i)
    i += 1
else:
    print("else a while-ban" , i )
print( "=== Itt a program vége ===" , i)
