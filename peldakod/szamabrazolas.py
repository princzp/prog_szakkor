# Számábrázolási "hibák": a kettes számrendszer következményei

from decimal import Decimal
#from fractions import Fraction

print ("Példa: 0.1+0.1+0.1-0.3=0 ?..")
print ("Ez nulla kellene legyen, de:" , ((.1+.1+.1)-(.3)) )
print ("Sőt, valójában.............:" , Decimal((.1+.1+.1)-(.3)) )
print ("De lehet kerekíteni........:" , round( Decimal((.1+.1+.1)-(.3)) , 6 ) )
print ( "A probléma: 0.1 valójában.:" , Decimal(0.1) )
print ( round(Decimal(0.1) ) )
print ( round(Decimal(0.1) , 0 ) )
print ( round(Decimal(0.1) , 1 ) )
print ( round(Decimal(0.1) , 2 ) )
print ( round(Decimal(0.1) , 3 ) )
print ( round(Decimal(0.1) , 4 ) )
print ( round(Decimal(0.1) , 5 ) )
print ( round(Decimal(0.1) , 6 ) )
print ( round(Decimal(0.1) , 7 ) )
print ( round(Decimal(0.1) , 8 ) )
# ...
print ( round(Decimal(0.1) , 16 ) )
print ( round(Decimal(0.1) , 17 ) )

print ( round(Decimal(0.1) , 20 ) )
