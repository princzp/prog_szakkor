def censor(text,word):
    censtext = []
    szavak = text.split()

    for szo in szavak:
        if ( szo.lower() != word.lower() ): censtext.append(szo)
        else: censtext.append("*" * len(word))
    return " ".join(censtext)

print ( censor("aaa 111 bbb 111 ccc 11 ddd 1111", "111") )
