# File I/O

"""
Irodalom:
    [PFK] 10. fejezet
    [Swin] 9. fejezet
    [Tut] 7., 10. és 11. fejezet
    [Down] 14. fejezet
"""

# ==========================================
# file alapműveletek
fileom = open("proba.txt","w")
fileom.write("Hello world write módban!\n")
fileom.close()

fileom = open("proba.txt","r+")
mivanbenne = fileom.read()
print(mivanbenne)

hanybetutirtam = fileom.write("Hello world\n")
print(hanybetutirtam)

mivanbenne = fileom.read()
print(mivanbenne)

fileom.close()

# ==========================================
# egyszerű json dump/load
# Program változóit ki tudjuk tenni file-ba és felolvasni onnan

import json
adat = ["hello", "world", 42]
print( json.dumps( adat ) , type( json.dumps( adat ) ) )

f = open("json.txt","w")
#f.write(json.dumps( adat ))
json.dump( adat , f )
f.close()

f = open("json.txt","r")
filebol=json.load(f)
print( filebol , type( filebol ) , type(filebol[2]))
f.close()

# Példa (torpedó játék állapotainak mentése):
# ["Anna", 3, {1:(3,3),2:(2,4)}]
# Anna játékosnak van még 3 torpedója, az első két lövés koordinátái: (3,3), (2,4)
# Tehát egy lista, benne egy string, egy szám és egy szótár,
# ahol a szótár kulcsa a lövés sorszáma, az érték pedig egy (sor,oszlop) tuple

# ==========================================
