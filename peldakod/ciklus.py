# ciklus próba

i = 10

print ("Ciklus indul.")

while i <= 20:
    if i == 13:
        print (":(")
        i+=1
        # i -= 2
        continue
        # break
    print (i)
    i += 1
else:
    print("While feltétel hamis: i =" , i)

print ("Ciklus vége.")

for j in range(10,21):
    if j==13 : continue

    print(j)
    
    # !!!
    j=0

