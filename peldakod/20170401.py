# 2017.04.01

"""
Ismétlés az első 4 alkalom anyagából.
Gondolkodtató feladatok.
"""

"""
Két változó értékének felcserélése.
Anélkül, hogy tudnánk, mi van a dobozokban.
Annyit tudunk, hogy mindkettő szám.
"""

# minden ötlet előtt vissza kell majd állítani őket...
a = 42
b = 3

# 1. ötlet: +2 változó bevezetése
print ("1. ötlet: +2 változó bevezetése")
print("a és b értéke:" ,a,b)
print("Csere...")
c = b
d = a

a = c
b = d

print("a és b értéke:" ,a,b,"\n")

# 2. ötlet: elég csak 1 változó!!!
print ("2. ötlet: elég csak 1 változó!!!")
a = 42
b = 3
print("a és b értéke:" ,a,b)
print("Csere...")

tmp = a
a = b
b = tmp

print("a és b értéke:" ,a,b,"\n")
 
# 3. ötlet: valamilyen invertálható művelet, pl. + és -
# Probléma lehet: túlcsordulás, alulcsordulás!
print ("3. ötlet: valamilyen invertálható művelet, pl. + és -")
print ("Probléma lehet: túlcsordulás, alulcsordulás!")
a = 42
b = 3
print("a és b értéke:" ,a,b)
print("Csere...")

a = a + b
b = a - b
a = a - b

print("a és b értéke:" ,a,b,"\n")

# 4. szintaktikus cukorka csak Pythonban
print("4. szintaktikus cukorka csak Pythonban")

"""
# ötlet: többszörös értékadás

x,y,z = 1,2,3 Python-ban ugyanaz mint:

x = 1
y = 2
z = 3
print(x,y,z)
"""
a = 42
b = 3
print("a és b értéke:" ,a,b)
print("Csere...")

a,b = b,a

print("a és b értéke:" ,a,b,"\n")

input("Program vége, nyomj Enter-t...")

"""
Elágazás gondolkodtató feladat
fizz buzz (róka-béka) játék
"""

# 1. megoldás: külön if-ek minden
# oszthatósági esetre, összesen 4 db.
print("RókaBéka, 1. változat")

kimenet = ""
for szam in range(1,101):
    szoveg = ""

    if (szam % 3 !=0 and szam % 5 != 0):
        szoveg = str(szam)

    if ( szam % 3 == 0):
        szoveg += "róka"

    if ( szam % 5 == 0):
        szoveg += "béka"

    if ( szam % 3 == 0 and szam % 5 == 0):
        szoveg = "rókabéka"
        
    kimenet += szoveg + ","
kimenet += "KÉSZ."
print (kimenet)

input("Program vége, nyomj Enter-t...")

# 2. megoldás fizzbuzz v2
# Kihasználni, hogy az oszthatósági esetek
# nem diszjunktak => akár >1 if-be is beengedni
# a programot...
print("RókaBéka, 2. változat")

mettol = 1
meddig = 100
kimenet = ""

for i in range(mettol,meddig+1):
    kimenet = ""
    
    if (i%5 !=0 and i%3 !=0): #(i%5 and i%3 )
        kimenet += str(i)
    
    # Ez az ág nem is kell
    # if (i%5 == 0 and i%3 == 0 ):
    #     output = "rókabéka" # + (" + str(i) + ")"
    
    if (i%3 == 0):
        kimenet += "róka" # + "(" + str(i) + ")"
    
    if (i%5 == 0):
        kimenet += "béka" # + "(" + str(i) + ")"
    # ...
    print( kimenet, end = "," )

print("KÉSZ.")
input("Program vége, nyomj Enter-t...")

"""
Ciklusokkal kapcsolatos gondolkodtató feladat
While és for is elöltesztelő.
Lehet-e Python-ban (vagy bármilyen nyelven)
hátultesztelő ciklust írni ezekkel az elöltesztelő
nyelvi elemekkel?
"""
# ciklusok, ismétlés

##i = 1
##while False: # i <= 5:
##    print(i)
##    i += 1

##for i in range(1,6,1):
##    print(i)
##    i = i+1 # nem kell, for elintézi

# Kérdés: hátultesztelő ciklus?

# Pl. ez:
    
i = 1
while i == 1:
    print (i)
    i += 1

# vagy ez:
feltetel = True
i = 1
while feltetel:
    print ("Még mindig igaz...", i)
    i += 1
    feltetel = (i<2)

# játékokban tipikus: látszólag végtelen ciklus, de van benne egy feltételes break...
i = 1
while True:
    print("végtelen?" , i)
    if (i==5):
        break
    i+=1

input("Program vége, nyomj Enter-t...")

"""
Függvény fogalma.
"""

def osszeg(a,b):
    print ("hello" , a , b )
    # ...
    return (a+b)

ketto = osszeg(1,1)

egyik_szam = osszeg(3,4)
masik_szam = osszeg(5,6)

print (ketto, egyik_szam, masik_szam)

# önálló feladat: nagyobb() fgv.
def nagyobb(a,b):
    if a>b:
        return a
    elif a == b:
        return a
    elif a<b:
        return b

def uresfgv():
    print ("hello from uresfgv")
    return (False)

print ( nagyobb(0,0), nagyobb(5,3), nagyobb(3,5) )
print ( uresfgv() )

input("Program vége, nyomj Enter-t...")

"""
Lista fogalma, lista szintaxisa Pythonban
"""

# lista általában homogén elemeket tartalmaz
lista1 = ["piros", "sárga", "zöld"]
print(lista1)

# ...de nem feltétlenül
lista2 = ["piros", 42 , 3.14, False]
print (lista2)

# enumerate() függvény
i=1
for szin in lista1:
    print(i, szin )
    i+=1

for i,elem in enumerate(lista2):
    print (i+1,elem)

input("Program vége, nyomj Enter-t...")

# string tördelése szóhatáron, szavak listájába
print ("hello world aaa banán".split())

# emlékeztető: string hossza
print ("hello hossza:" , len("hello"))

# emlékeztető: string többszörözése
print ("Öt csillag:" , "*" * 5)

# a kettőt összerakni: string ismételgetése olyan hosszan, amilyen hosszú egy máasik string
print ("Banán -> " , "*" * len("banán"))
print ("Zöldbanán ->" , "*" * len("zöldbanán"))

# szavak listáját összefűzni egyetlen stringbe, elválasztó stringgel
print (",".join("hello world aaa banán".split()))

# Lista rendezése

lista1 = ["a" , "x" , "y" , "z" , "b" , "c"]
print ("Eredeti lista:   " , lista1)
 
lista1.sort()
print ("Helyben rendezve:" , lista1)
 
# visszaállítani rendezetlenre
lista1 = ["a" , "x" , "y" , "z" , "b" , "c"]

# Vigyázat: lista csak []-kor jön létre
# változó másolással NEM!
# Ilyenkor csak hivatkozás másolódik az EREDETI listára!!!
lista2 = lista1
print ("lista1: ", lista1)
print ("lista2: ", lista2)

print("Most rendezem lista2-t")
print("Látszólag nem nyúlok lista1-hez...")

lista2.sort()
# Upsz, nem csak lista2 lett rendezett, hanem lista is!!!

print ("lista1: ", lista1)
print ("lista2: ", lista2)

input("Program vége, nyomj Enter-t...")

"""
Önálló munka: cenzor() fgv. megírása: stringből adott mintának megfelelő szót kisípolni.
"""
def kisipol(szoveg, minta):
    szavak = szoveg.split()
    eredmeny = []
 
    for szo in szavak:
        if szo != minta:
            eredmeny.append(szo)
        else:
            eredmeny.append("*" * len(minta))
    return (eredmeny)
 
print ( kisipol ("alma körte banán" , "körte") )
print ( kisipol ("aaa bbb ccc aaa ddd", "aaa"))

