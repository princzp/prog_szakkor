"""
    2017.05.20 
    Objektum-orientált paradigma
"""


"""
Irodalom (OO):
    [PFK] 8. fejezet
    [Swin] 11. és 12. fejezet
    [Tut] 9. fejezet
    [Down] 15.-18. fejezet
"""

"""
https://hu.wikipedia.org/wiki/Madagaszkár_(film)

Alex (oroszlán)                     	        Ben Stiller	            Zámbori Soma
Marty (zebra)                   	        Chris Rock/Phil LaMarr	    Molnár Levente
Melman (zsiráf)                 	        David Schwimmer             Seder Gábor
Gloria (víziló)                 	        Jada Pinkett Smith	    Liptai Claudia
Julien király (gyűrűsfarkú maki)	        Sacha Baron Cohen	    Galbenisz Tomasz
Maurice (véznaujjú maki)        	        Cedric the Entertainer	    Beregi Péter
Mort (egérmaki)                 	        Andy Richter	            Arany Tamás
Kapitány (angolul Skipper) (pingvin)	        Tom McGrath	            Reviczky Gábor
Közlegény (pingvin)             	        Christopher Knights	    Katona Zoltán
Kowalski (pingvin)              	        Chris Miller	            Kerekes József
Rico (pingvin)                  	        John DiMaggio	            (nem szólal meg)
Mason (csimpánz)	                        Conrad Vernon	            Berzsenyi Zoltán

Rendőrségi ló (ló)	                        David Cowgill	            Harmath Imre
Rendőrtiszt (aki a lovon ült)	                Stephen Apostolina	    Juhász György
Idős hölgy (aki a táskával verte Alex-et)	Elisa Gabrielli	            Várnagy Katalin
Pánikoló férfi a metrón                 	Tom McGrath	            Lukácsi József
Metró-hangosbemondó	                        Eric Darnell	            Szalóczy Pál
Állatkerti hangosbemondó	                Eric Darnell                Orosz István
Makik	                                                                    Bede-Fazekas Szabolcs
                                                David P. Smith	            Kapácsy Miklós
Pók	                                                                    Szokol Péter
Willie	                                        Cody Cameron	            Németh Gábor
Híradós riporter	                        Devika Parikh	            Téglás Judit
"""

# ==========================================
# [Tut] 9. fejezet, https://docs.python.org/3/tutorial/classes.html


"""
                       +------------------+
                       |  Állat           |
                       |                  |
                       +------------------+
                       | lélegzik()       |
             +---------> mozog()          <-------------------+
             |         | táplálkozik()    |                   |
             |         |                  |                   |
             |         +------------------+                   |
             |                                                |
             |                                                |
             |                                                |
             |                                                |
             |                                                |
     +-----------------+                            +------------------+
     |  Madár          |                            | Emlős            |
     +-----------------+                            +------------------+
     |                 |                            |                  |
     | tojást_rak()    |                            | szoptat()        |
     |                 |                            |                  |
     +--------^--------+                            +---------^--------+
              |                                               |
              |                                               |
     +------------------+                            +------------------+
     | pingvin          |                            | zsiráf           |
     +------------------+                            +------------------+
     |                  |                            |                  |
     |                  |                            |                  |
     |                  |                            |                  |
     +------------------+                            +------------------+
"""

class allat:
    #pass

    def lelegzik(self):
        #pass
        print(self, "lélegzik (mert állat)")

    def mozog(self):
        #pass
        print(self, "mozog (mert állat)")

    def taplalkozik(self):
        #pass
        print(self, "eszik (mert állat)")

# öröklődés
class madar(allat):
    def tojast_rak(self):
        print(self, "tojást rak (mert madár)")

class emlos(allat):
    def szoptat(self):
        print(self, "szoptat (mert emlős)")

class pingvin(madar):
    pass

class zsiraf(emlos):
    # pass
    def __init__(self, foltok_szama):
        self.zsiraf_folt = foltok_szama
        print("Létrejött egy zsiráf objektum %d folttal" % (foltok_szama) )

    def elelmet_keres(self):
        print(self, "élelmet keres (mert zsiráf)")
        self.mozog()
        print(self, "élelmet talált (mert zsiráf)")
        self.taplalkozik()

Kapitany    = pingvin()
Rico        = pingvin()
Kozlegeny   = pingvin()
Kowalski    = pingvin()

#Melman      = zsiraf()
Melman      = zsiraf(42)
zsiraf2     = zsiraf(100)

# példa polimorfizmusra
Rico.lelegzik()
Kapitany.mozog()
Rico.mozog()

Melman.taplalkozik()
Melman.elelmet_keres()

Kozlegeny.tojast_rak()
Melman.szoptat()

#! AttributeError: az osztálydiagram szerint lehetetlen esetek
# Kowalski.szoptat()
# Melman.tojast_rak()

input("Üss Enter-t a folytatáshoz!")
# ==========================================

# ==========================================
class pont:
    def __init__(self,x,y):
        # Mivel van get/set, inicializálhatnánk None-ra is
        self._x = x
        self._y = y

    def kiir(self):
        print("A %s pont koordinátái: (%d,%d)" % (self, self._x , self._y) )

    # Property, getter/setter: https://docs.python.org/3/library/functions.html#property
    def get(self):
        return (self._x, self._y)

    def getx(self):
        return (self._x)

    def gety(self):
        return (self._y)

    def set(self,x,y):
        self._x = x
        self._y = y

egyikpont = pont(10,20)
masikpont = pont(13,24)

from math import sqrt
def tavolsag(p1, p2):
    """
    Kiszámolja két pont távolságát
    Pitagorasz-tétel alkalmazása
    >>> print( tavolsag( pont(0,0), pont(3,4) ) )
    5.0
    """

    return ( sqrt( (p1.getx()-p2.getx())**2+(p1.gety()-p2.gety())**2 ) )

print("egyikpont koordinátái: " , egyikpont.get() )
print("masikpont koordinátái: " , masikpont.get() )
print("A két pont (objektum) távolsága: " , tavolsag( egyikpont, masikpont ) )

pont1 = pont(5,5)
pont2 = pont(5,5)
pont3 = pont1

print("pont1 koordinátái: " , pont1.get() )
print("pont2 koordinátái: " , pont2.get() )
print("pont3 koordinátái: " , pont3.get() )

pont1.kiir()
pont2.kiir()
pont3.kiir()

print( "Egyenlőségvizsgálat: " , egyikpont == masikpont , pont1 == pont2, pont1 == pont3 )

pont1.set(6,6)
print("pont1 koordinátái: " , pont1.get() )
print("pont2 koordinátái: " , pont2.get() )
print("pont3 koordinátái: " , pont3.get() )

input("Üss Enter-t a folytatáshoz!")
# ==========================================

# ==========================================
# Példányosításra példa: turtle rajzolás
import turtle
Franklin    = turtle.Pen()
Cool        = turtle.Pen()

Franklin.color("blue")
Cool.color("red")

Franklin.forward(100)
Cool.left(90)
Cool.forward(100)

# ==========================================

# ==========================================
# Objektumok kompozíciója
# A háromszög objektum három darab pont objektumból áll
# Nem vizsgáljuk, egy szakaszra esnek-e.
class Haromszog:
    def __init__(self, p1,p2,p3):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3

    def getp1(self):
        return (self._p1)

    def getp2(self):
        return (self._p2)

    def getp3(self):
        return (self._p3)

    def rajzol(self,t):
        t.penup()
        t.goto( self.getp1().getx(), self.getp1().gety() )
        t.pendown()

        t.goto( self.getp2().getx(), self.getp2().gety() )
        t.goto( self.getp3().getx(), self.getp3().gety() )
        t.goto( self.getp1().getx(), self.getp1().gety() )

h1 = Haromszog(pont(100,100),pont(200,200),pont(300,100))
h1.rajzol(Franklin)

h2 = Haromszog(pont(-100,100),pont(-200,200),pont(-300,100))
h2.rajzol(Cool)

input("Üss Enter-t a folytatáshoz!")
# ==========================================

# ==========================================
# Polimorfizmus példa
# N.B. a legelső, Madagaszkáros is tkp. polimorfizmus
print ("Polimorfizmus példa")

class allat:
    def __init__(self, nev):
        self._nev = nev

    def hangot_ad(self):
        print (self._nev , "azt mondja" , self.hang() )

class tehen(allat):
    # pass
    def hang(self):
        return "múúú"

class csacsi(allat):
    # pass
    def hang(self):
        return "iááá"

class kutya(allat):
    # pass
    def hang(self):
        return "vau-vau"

class beszelo_kutya(kutya):
    def hangot_ad(self):
        super().hangot_ad()
        print ("Ezen felül," , self._nev , ", a beszélő kutya még beszélni is tud")


riska = tehen("Riska")
riska.hangot_ad()

csacsi("Rocinante").hangot_ad()

kutya("Bodri").hangot_ad()
#kutya("Jeromos").hangot_ad()
beszelo_kutya("Jeromos").hangot_ad()

input("Üss Enter-t a folytatáshoz!")
# ==========================================

# ==========================================
# Absztrakt osztályok és metódusok példa
# Motiváció: klasszikus példa, síkidomok
# https://docs.python.org/3/library/abc.html

print ("Absztrakt osztályok és metódusok példa")

from abc import abstractmethod
from math import pi, sqrt

class sikidom(object):

    @abstractmethod
    def kerulet(self):
        # pass
        """
        A síkidom kerületét implementáló absztrakt metódus.
        Ne felejtsd el a metódust is példányosítani!
        """
        raise (NotImplementedError("Kerületet nem implementáltad. %s" % (self)))

    @abstractmethod
    def terulet(self):
        # pass
        """
        A síkidom területét implementáló absztrakt metódus.
        Ne felejtsd el a metódust is példányosítani!
        """
        raise (NotImplementedError("Területet nem implementáltad. %s" % (self)))

class kor(sikidom):
    # pass
    def __init__(self,r):
        # nem kirajzolni akarjuk, mindegy, hogy hol van, csak a mérete
        self._r = r

    def kerulet(self):
        return (2*self._r*pi)

    def terulet(self):
        return (self._r**2*pi)

class haromszog(sikidom):
    # pass
    # háromszög területe oldalakból: Heron képlet 
    # https://hu.wikipedia.org/wiki/Hérón-képlet
    def __init__(self,o1,o2,o3):
        self._o1 = o1
        self._o2 = o2
        self._o3 = o3

class teglalap(sikidom):
    # pass
    def __init__(self,o1,o2):
        self._o1 = o1
        self._o2 = o2

k1 = kor(5)
k2 = kor(10)

h1 = haromszog(3,4,5)
h2 = haromszog(10,20,29)

t1 = teglalap(10,20)
t2 = teglalap(30,30)

print ( k1.kerulet(), k1.terulet() )
print ( k2.kerulet(), k2.terulet() )

# Itt lehet megérteni, hogy kikényszeríti a hiányzó metódusok implementációját
# NotImplementedError: Kerületet nem implementáltad. <__main__.haromszog object at 0x7f8d26591470>
# print ( h1.kerulet(), h1.terulet() )
# print ( t1.kerulet(), t1.terulet() )

input("Üss Enter-t a folytatáshoz!")
# ==========================================
