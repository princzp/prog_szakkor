# ======================================
# 2017.04.22

# Stringek: tutorial 3.1.2

# ======================================
# string konkatenálás: stringek összege
# összeragasztás szóköz hozzáadása nélkül
s1 = "róka"
s2 = "béka"
print( "+" , s1 + s2 )
print( "," , s1 , s2 )

# plusz jel nélkül is, egymás mellé írva
print(" " , "béka" "róka")
# ======================================

# ======================================
# ismétlés (string szorzata)
print( "-" * 10 )
print( 10 * "+" )

# a kettő együtt: stringszorzatok összege
print( "-" * 10 + 10 * "+" )
# ======================================

# ======================================
# string hossza
s1 = "Hello, world!"
s2 = "-" * 10
print( len(s1) , len(s2) )

# Szépen aláhúzni egy stringet, függetlenül a hosszától
print( s1 )
print( "=" * len(s1) )
# ======================================

# ======================================
# indexelés
"""
    hello
    01234
"""
print("hello"[0])
print("hello"[1])
print("hello"[2])
print("hello"[3])
print("hello"[4])

# index error: 5 hosszú, indexek 0..4
# print("hello"[8])

print("visszafelé")
# hozzáadja len()-t, tehát bal szél [0], jobb szél [-1]
print("hello"[-1])
print("hello"[-2])
print("hello"[-3])
print("hello"[-4])
print("hello"[-5])

# index error
# print("hello"[-6])
# ======================================

# ======================================
# kivágás, szeletelés
# [eleje:vége], ahol:
# eleje már benne van, vége már nincs benne!!!
s = "Monty_Python!"
print(s, "hossza:" , len(s))
print( s[0:5] , s[5:12] , s[11])
print( s[0] , s[len(s)-1] , s[0:len(s)])

# valójában: [eleje:vége:lépésköz]
# alapértelmezésként +1, és nem lehet 0, de lehet negatív
print("Minden második betű: " , s[0:len(s):2])

# eleje is elhagyható
print("Eleje elhagyható:" , s[:5:1])
print("Eleje elhagyható:" , s[:5])
print("Eleje elhagyható:" , s[:5:])

# vége is elhagyható
print("Vége elhagyható:" , s[5::1])
print("Vége elhagyható:" , s[5:])
print("Vége elhagyható:" , s[5::])

# invariáns: s[:k]+s[k:]==s
print ("Invariáns (3):" , s[:3]+s[3:]==s)

# sőt ez minden k-ra teljesül
inv = True
for i in range(0,len(s)):
    inv = inv and s[:i]+s[i:]==s
    print (i, "/", len(s)-1,"invariáns:" , inv)
print ("Invaráns a legvégén:" , inv)

# tkp. mindhárom elhagyható: eleje, vége, lépés
print("Mindhárom elhagyható:" , s[::])

# Kérdés: string megfordítása?
print(s , "megfordítva:" , s[::-1])

# szeletelésnél megengedi a kicímzést
# csak direkt túl indexelésnél büntet...
# print (s, len(s), s[100]) # ez hiba
print (s, len(s), s[:100]) # ez nem hiba

# ======================================
# string bejárása for .. in iterátorral
print("String bejárása for ciklussal")
s = "Monty Python"

for betu in s:
    print (betu)

for i,betu in enumerate(s):
    print (i,betu)
# ======================================

# ======================================
# tartalmazás vizsgálata
s = "Monty Python"
if "ty P" in s: print("Benne van")
if "aa" not in s: print("Nincs benne")
# ======================================

# ======================================
# összehasonlítás: betűrendben
s1 = "hello"
s2 = "world"
print (s1, s2, s1==s2, s1<s2, s1<=s2, s1>s2, s1>=s2, s1!=s2)
s2 = "hello"
print (s1, s2, s1==s2, s1<s2, s1<=s2, s1>s2, s1>=s2, s1!=s2)
# ======================================

# ======================================
# változó/érték behelyettesítése string-be
pontok = 100
eletek = 3
jatekos = "Tibi"

# hagyományos módszer
print( "Van még " + str(eletek) + " életed és van " + str(pontok) + " pontod, " + jatekos + "!" )

# változó/érték behelyettesítése string-be: %d, %s, ...
print( "Van még %d életed és van %d pontod, %s!" % (eletek, pontok, jatekos) )
print( "Van még %d életed és van %d pontod, %s!" % (5, 200, "Anna") )

# egy értéket nem kell zárójelbe tenni
print( "Hello %s" % "Kati")

# kettőt már igen
print( "Hello %s %s" % ("Kati" ,"Feri") )
# ======================================

# ======================================
# stringek típuskényszerítése: int(), float()
s = "42"
print(s,type(s), int(s), type(int(s)))
print(s*2 , int(s)*2)

s = "4.2"
print(s,type(s), float(s), type(float(s)))
print(s*2 , float(s)*2)
# ======================================

# ======================================
# a stringek indexeléssel nem változtathatók meg
s1="hello"
print(s1[1])

# ez típushiba
# s1[1]="a"

# ez jó
# NB. viszont másol, dupla memóriát használ
s2 = s1[0] + "a" + s1[2:]
print (s1,s2)
# ======================================

# ======================================
# string metódusok (rengeteg, [Tut] 4.7.1)
print("hEllO","hello".lower(),"hello".upper())

s = "Hello, world! Monty Python!"
print(s)
print(s.lower())
print(s.upper())

print(" hello ".center(20,"="))
# ======================================

# Listák: tutorial 3.1.3
# Teljesen hasonlóak a stringekhez
# hiszen ez is szekvencia típus

# ======================================
l1 = ["alma", "körte", "banán"]
l2 = ["kutya","macska","ló"]
l3 = [3.1415926, 42 , 2.71828]

print(l1)
print(l2)
print(l1+l2)
# nem kell azonos típusúnak lenni az elemeknek
print(l1+l3)

# ugyanúgy lehet indexelni és szeletelni
print(l1[1] , l2[1:], l3[-1])

print("körte"[1]) # string[n] a string n. betűje
print( l1 , type(l1) )
print( l1[1] , type(l1[1]) ) # de lista[n] a lista n. eleme, ami nem betű, hanem listaelem
print( l1[1][1] , type(l1[1][1]) ) # ha az egy string volt, akkor újab [m] index már annak m. betűjét indexeli

# gyakori: üres, valamekkora lista létrehozása
print([0] * 12)

# a stringgel ellentétben a lista elemei felülírhatók
print(l2)
l2[2] = "egér"
print(l2)

# számok listája range()-el, list() fgv.
l3 = ( range(5) )
print(type(l3),l3)
l4 = list( range(7) )
print(type(l4), l4)

# lista hossza
print(l1+l2, len(l1+l2))

# lista bejárása for .. in iterátorral
for elem in (l1+l2):
    print (elem)

for i,elem in enumerate(l1+l2):
    print (i,elem)

# elemek lista végére fűzése
print(l1, len(l1))
l1.append("eper")
print(l1, len(l1))

# tartalmazás vizsgálata: in, not in
print(l1)
if "eper" in l1: print("Van benne eper")
if "mangó" not in l1: print("Nincs benne mangó")

"""
N.B. Python csak akkor hozza létre a listát, amikor a [] jeleket látja...
Nem úgy, mint skalár változóknál...
"""
v1 = v2 = 42
print (v1,v2)
v1 = "hello"
print (v1,v2)

l1 = l2 = ["a","b","c"]
print (l1,l2)
l1.append("x")
print (l1,l2)

# Két külön listaváltozó: slice vagy list().
l1 = ["a","b","c"]
l2 = l1[:]
l3 = list(l1)
print (l1,l2,l3)
l1.append("x")
l2.append("y")
print (l1,l2,l3)
# NB. van a metódusok között copy() is

# Tipp: a reverse() gyors, lista elejére beszúrni drága. => reverse()-zel átmenetileg megtükrözni, végére írni, majd visszafordítani...

# többdimenziós listák: listák egymásba ágyazása
linl = [ ["a", "b", "c"] , ["x","y","z"] ]
print (type(linl), len(linl), linl)

# Példa többdimenziós listára. (Sakktábla: 2 dimenziós.)
sor = [0] * 8
sakktabla = [sor] * 8
print ("Sakktábla (csupa nulla):" , sakktabla)

sakktabla2 = []
for i in range(8): sakktabla2.append([i]*8)
print("Sakktábla (jobban olvasható):" , sakktabla2)

# Sakktábla pretty print
for i  in range(len(sakktabla2)): print(sakktabla2[i])
# ======================================

