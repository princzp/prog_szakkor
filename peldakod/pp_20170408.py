# http://www.mathematische-basteleien.de/eggcurves.htm

from turtle import *
from math import sqrt
reset()

teknos = Turtle()
teknos.penup()
teknos.hideturtle()
#teknos.speed(0)
delay(0)

# túl gyors, de ez is egy lehetőség
# tracer(0,0)

def szalkereszt(t,mekkora):
    t.hideturtle()
    # t.goto(0,0) ; t.pendown() ; t.dot(5) ; t.penup()
    egypont(t,0,0,5)
    t.goto(-mekkora,0) ; t.setheading( 0) ; t.pendown() ; t.forward(2*mekkora) ; t.penup()
    t.goto(0,-mekkora) ; t.setheading(90) ; t.pendown() ; t.forward(2*mekkora) ; t.penup()

    return (True)

def egypont(t,x,y,meret):
    t. hideturtle()
    t. goto(x,y)
    t.pendown()
    t.dot(meret)
    t.penup()

    return (True)

# ======================== KÖR ========================== 
def kor(t,x0,y0,r):
    # t.hideturtle()

    for x in range(x0-r,x0+r+1):

        dy = sqrt (r**2 - (x-x0)**2)

        y1 = y0 + dy
        y2 = y0 - dy

        #t.goto(x,y1) ; t.pendown() ; t.dot(1) ; t.penup()
        #t.goto(x,y2) ; t.pendown() ; t.dot(1) ; t.penup()

        egypont(t,x,y1,1)
        egypont(t,x,y2,1)


    return (True)

# ==================== ELLIPSZIS =======================
def ellipszis(t,x0,y0,r1,r2):
    # t.hideturtle()
    
    for x in range(x0-r1,x0+r1+1):
        
        dy = sqrt ((r1**2 * r2**2 - r2**2 * (x-x0)**2) / (r1**2))
        y1 = y0 + dy
        y2 = y0 - dy

        # t.goto(x,y1) ; t.pendown() ; t.dot(1) ; t.penup()
        # t.goto(x,y2) ; t.pendown() ; t.dot(1) ; t.penup()

        egypont(t,x,y1,1)
        egypont(t,x,y2,1)

    return (True)

# ==================== TOJÁS =======================
def tojas(t,x0,y0,r1,r2):
    # t.hideturtle()
##    szin = t.pencolor()
    
    for x in range(x0-r1,x0+r1+1):

        t_x = 1+0.3*(x-x0)/r1
        dy = sqrt ((r1**2 * r2**2 - r2**2 * (x-x0)**2) / (r1**2) / t_x )
        
        y1 = y0 + dy
        y2 = y0 - dy

##        t.pencolor(szin)
        # t.goto(x,y1) ; t.pendown() ; t.dot(1) ; t.penup()
        # t.goto(x,y2) ; t.pendown() ; t.dot(1) ; t.penup()

        egypont(t,x,y1,1)
        egypont(t,x,y2,1)

##        t.pencolor("brown")
##        t.goto(y1,x) ; t.pendown() ; t.dot(1) ; t.penup()
##        t.goto(y2,x) ; t.pendown() ; t.dot(1) ; t.penup()

##        egypont(t,y1,x,1)
##        egypont(t,y2,x,1)


    return (True)

# ================ FŐPROGRAM ==========================

#kor       (teknos, -200 ,  300 , 100)
#kor       (teknos,  200 ,  300 , 100)
#ellipszis (teknos, -200 ,    0 , 150 , 200)
#ellipszis (teknos,  200 ,    0 , 200 , 150)
#tojas     (teknos, -200 , -300 , 150 , 200)
#tojas     (teknos,  200 , -300 , 200 , 150)

teknos.pencolor("black") ; szalkereszt(teknos,300) # ; input()

teknos.pencolor("green") ; kor      (teknos, 0 , 0 , 200) # ; input()
teknos.pencolor("blue")  ; ellipszis(teknos, 0 , 0 , 200 , 150) # ; input()
##teknos.pencolor("blue")  ; ellipszis(teknos, 0 , 0 , 150 , 200) # ; input()

#teknos.fillcolor("brown") ; teknos.begin_fill()
teknos.pencolor("red")   ; tojas    (teknos, 0 , 0 , 200 , 150) ;
# teknos.end_fill()

#teknos.pencolor("brown") ; ellipszis(teknos, 0 , 0 , 210 , 210)

#teknos.update()
