# ======================================
# 2017.04.29
# ======================================

# tuple: tutorial 5.3
# ======================================
t1 = ("tej", 290 , "Ft" , 2 , "l")
# nem is a zárójel a lényeg, hanem a vessző
t2 = 42, 3.1415926, "alma", 2.71828
print (t1)
print (t2)

# 1 elemű tuple furcsa, de így van:
v3 = 42
t3 = 42,
print (v3, type(v3), t3, type(t3))

# üres tuple
t4 = tuple()
print( t4, type(t4))

# tuple()-ben szekvencia
t5 = tuple("hello")
print(t5, type(t5))

# szeletelés
print(t2)
print(t2[:2])

# két tuple összehasonlítása
print ( (1,2,3) < (1,2,4) , (2,2,3) < (1,2,4) )

# string tördelése tuple-be
email = 'monty@python.org'
uname, domain = email.split('@')

print( "email :" , email )
print( "user  :" , uname )
print( "domain:" , domain)

# több visszatérési érték fgv.ből: tuple!
print( divmod(7,3) )
hanyszor, maradek = divmod(7,3)
print("Hányszor:", hanyszor)
print("Maradék :" , maradek)

# feladat: min_max fgv.
def min_max(par):
    return min(par), max(par)
print(min_max(["alma", "körte","banán"]))

# szekvenciák cippzárazása tuple-be
s = "alma"
l = [42,3.14,2.71,0]
print (type(zip(s,l)), zip(s,l))

# cippzár bejárása for ciklussal
for par in zip(s,l): print (par)

# cippzár tuple-listává konvertálása
print(list(zip(s,l)))

# egyszerre több szekvencia iterálása
"""
A zip, for és tuple értékadás segítségével egyszerre lehet bejárni több szekvenciát.
Pl. egyezik() visszaad igazat, ha van olyan i index,
ahol a két szekvencia azonos értékű (seq1[i] == seq2[i])
"""
def egyezik(seq1, seq2):
    for x, y in zip(seq1, seq2):
        if x == y:
            return True
    return False

l1 = ["alma", "körte", "banán"]
l2 = ["eper", "körte", "mangó"]
l3 = ["eper", "kiwi" , "mangó"]
print (egyezik(l1,l2) , egyezik(l1,l3))
# ======================================

# Dictionary (szótár): tutorial 5.5.
# ======================================
# szótár létrehozása
d1 = {}
d2 = {"Zénó":123 , "Anna":234, "Ubul":345}
print( type(d1), d1 , type(d2), d2)

# szótár kulcsainak listája, rendezett listája
print( list( d2.keys() ) ) # NB. akármilyen sorrendben jöhet
print( sorted( d2.keys() ) )

# kulcs tartalmazás ellenőrzése szótárban
print ("Ubul" in d2 , "Barbi" not in d2)

# szótárhoz hozzáírás
d2["Barbi"] = 456
print(d2)

# szótárban felülírás
d2["Barbi"] = 567
print(d2)

# törlés szótárból
del(d2["Barbi"])
print(d2)

# szótár értékeinek listája
print( list( d2.values() ) )

# kulcshoz tartozó érték olvasása szótárból
print( d2["Ubul"] )

# key error!
# print (d2["ubul"] )

# ======================================

# Set (halmaz): tutorial 5.4
# ======================================

# halmaz létrehozása
# NB. az elemek egyszer szerepelhetnek
h1 = {"alma" , "körte", "banán", "alma"}
print( type(h1), h1 )

# üres halmaz létrehozása
# NB. h2={} nem jó, az üres *dictionary*-t hoz létre
h2 = set()
nemh2 = {}
print(type(nemh2), nemh2 , type(h2), h2)

# halmazhoz tartozás ellenőrzése
print(h1)
print("alma" in h1, "eper" not in h1)

# halmazműveletek
h1 = {"alma", "körte", "banán"}
h2 = {"kombinált fogó" , "fázisceruza" , "körte" }
print ("h1:" , h1)
print ("h2:" , h2)

# unió
print( h1 | h2 )

# metszet
print( h1 & h2 )

# különbség
print (h1-h2 , h2-h1)

# szimmetrikus differencia
print( h1 ^ h2 )

# ======================================

