# 2017.04.29
# számkitalálós játék

# A gép kitalál egy véletlen számot 1 és 10 között
from random import randint
gondolt_szam = randint(1,10)

print ("Gondoltam egy számot 1 és 10 között. Találd ki!")

# Ezt éles játékban ki lehet venni
print ("Súgok: a %d számra gondoltam..." % gondolt_szam)

tippek_meg = 3

while tippek_meg > 0:
    print("Mi a tipped? (Van még " + str(tippek_meg) + "...)")
    jatekos_tippje = int(input())

    # Kicsit elegánsabban ugyenez:
    # jatekos_tippje = int(input("Tipped (van még %d): " % tippek_meg ))

    if gondolt_szam == jatekos_tippje:
        print("Gratulálok! Nyertél!")
        break
    # Game over 1. változat: elif ág
    # nem mindegy a sorrend, könnyű elszúrni
    # elif tippek_meg == 1 and gondolt_szam != jatekos_tippje:
    #   print("Vége, nincs több tipped. (elif)")

    elif gondolt_szam > jatekos_tippje:
        print("Nagyobb számra gondoltam.")
    elif gondolt_szam < jatekos_tippje:
        print("Kisebb számra gondoltam.")

    # Game over 2. változat: önálló if
    # talán a legbiztonságosabb megoldás
    if tippek_meg == 1 and gondolt_szam != jatekos_tippje:
       print("Vége, nincs több tipped. (if)")

    tippek_meg = tippek_meg - 1
# Game over 3. változat: else
# szuper, mert nem kell feltételt írni
# de csak Python-ban van ilyen,
# más nyelvben nem feltétlenül van else ága a ciklusoknak
#else:
#    print("Vége, nincs több tipped. (else)")

print ("...Vége a játéknak...")
