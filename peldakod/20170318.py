# 2017. március 18.

"""
Elágazás példaprogram.
Egyszerű if...else, 2 ágú.
"""
szoveg= input("Gépelj be egy számot! ")
x = float(szoveg)

# ugyanez 1 sorban
# (szoveg nem is kell változóba):
# x = float(input("Gépelj be egy számot! "))

print(x)

if x<0:
    print( "Negatív" )
    print( "if ág vége." )
else:
    print( "Nemnegatív" )
    print( "else ág vége." )

print ( "Program vége" )

"""
Több ágú, if...else: if...-fel.
Látszik, hogy ronda...
"""
szin = "piros"

if szin == "piros":
    #mindegy
    pass
else:
    if szin == "pirossarga":
        pass
    else:
        if szin == "sarga":
            pass
        else:
            if szin == "zold":
                pass
            else:
                pass
            
"""
Az elif kulcsszó bevezetése.
Szintaktikus cukorka az egyre mélyebb tabulálások elkerülésére.
"""

if szin == "piros":
    pass
elif szin == "pirossarga":
    pass
elif szin == "sarga":
    pass
elif szin == "zold":
    pass

"""
Int és bool kapcsolata:
0 = hamis, minden más igaz...
"""

if ((2+4)-6):
    print("Igaz ág")
else:
    print("Hamis ág")

