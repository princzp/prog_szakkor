# 2017.04.08 szakkör

"""
Változók láthatósága és élettartama.
Névtér fogalma.
Global kulcsszó.
"""

def fuggveny(x):
    lokalis = "lokális"

    # Lokális a változó létrehozása
    # Elfedi a globálisan látható a-t.
    # (Napfogyatkozás példa)
    # a = 0  

    # Amikor nem szeretnék lokáslis változót
    global a
    
    print("A változók értékei a függvényben:" , a , lokalis , x )
    x = x + 1

    # ez vagy a lokális a-t növeli, ha létrehoztuk
    # vagy a globálisat a főprogramban,
    # ha a global kulcsszót használtuk
    # Próbáld ki mindkettőt és hasonlítsd
    # össze az eredményt!
    a = a + 1

    return (x)

# HIBA!!! Nem látszik, mert már megszűnt!!!
# print(lokalis) 

a = 42
print ("a a főprogramban:" , a)
print ("5-re azt mondja:" , fuggveny(5) )
print ("a-ra azt mondja:" , fuggveny(a) )

print ("a a főprogramban:" , a)

input("Program vége, nyomj Enter-t...")

"""
Kör, ellipszis és tojás rajzolása teknőcgrafikával.
(Nem sikerült befejezni.)
"""

from turtle import *
from math import sqrt
reset()

# Hogy gyors legyen a rajzolás
delay(0)

# Ezzel már túl gyors, de ez is egy lehetőség
# tracer(0,0)

# Példa
# franklin = Turtle()
# franklin.pensize(3)

teknos = Turtle()
teknos.pensize(1)
teknos.penup()
teknos.hideturtle()

# 1. lépés: egy sima körvonal
def kor(t,x,y,r):
    for i in range(x-r,x+r+1,1):
        dy = sqrt (r**2 - (i-x)**2)
        
##        penup()
##        goto(i,y+dy)
##        pendown()
##        dot(1)
        egypont(t,i,y+dy)

##        penup()
##        goto(i,y-dy)
##        pendown()
##        dot(1)
        egypont(t,i,y-dy)

        #print(i,y, dy)
    
    return (True)

# 2. lépés refaktorálás
def egypont(t,x,y):
    t.penup()
    t.goto(x,y)
    t.pendown()
    t.dot(1)
    # t.dot(t.pensize())

    return (True)

def szalkereszt(t,x,y,meret):
    t.penup()
    t.goto(x-meret,y)
    t.setheading(0)
    t.pendown()
    t.forward(2*meret)

    t.penup()
    t.goto(x,y+meret)
    t.setheading(270)
    t.pendown()
    t.forward(2*meret)

    
    return (True)

szalkereszt(teknos,0,0,200)

teknos.pencolor("blue")
teknos.pensize(3)
kor(teknos,50,50,100)

teknos.pencolor("red")
teknos.pensize(5)
kor(teknos,100,100,50)

