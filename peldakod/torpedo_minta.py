# Torpedó játék egy lehetséges változata

from random import randint
from math import sqrt
import os

SOROK    = 9    # 1..9 között
OSZLOPOK = 7    # 1..9 között
LOVESEK  = 15
jatekmezo = []

for x in range(SOROK):
    # print ([1, 2, 3] * 3)
    jatekmezo.append(["."] * OSZLOPOK)

def print_jatekmezo(jatekmezo):
    # képernyőtörlés linux-on
    # windows-on szinte ugyanez,
    # csak *állítólag* "cls" a parancs "clear" helyett
    # TODO: letesztelni, mert nem tudom kipróbálni
    os.system("clear")
    print ("Torpedó játék, %dx%d méretű tábla, %d lövés" % (SOROK, OSZLOPOK, LOVESEK))

    # Segítség a találathoz, éles játékból majd kivenni!!!
    print ("Segítség teszteléshez: a hajóm koordinátái: %d. sor, %d. oszlop" % (hajo_sor+1, hajo_oszlop+1) )

    # Játékmező fejléccel.
    # Első megközelítés
    # print ("  1 2 3 4 5")
    # print ("  ---------")

    fejlecsor = ""
    for i in range(OSZLOPOK): fejlecsor += str(i+1) + " "

    print ("  " + fejlecsor)
    print ("  " +  ("-" * (len(fejlecsor))))

    for i,sor in enumerate(jatekmezo):
        #print (sor)
        print ( str(i+1) + "|" + (" ".join(sor)) )

def random_sor(jatekmezo):
    # olvashatná a globális változót is, paraméter nélkül
    # SOROK
    return randint(0, len(jatekmezo) - 1)

def random_oszlop(jatekmezo):
    # olvashatná a globális változót is, paraméter nélkül
    # OSZLOPOK
    return randint(0, len(jatekmezo[0]) - 1)

def tavolsag( sor1, oszlop1, sor2, oszlop2 ):
    """
    Két (x,y) koordinátával adott pont távolságát számolja ki.
    >>> tavolsag( 1, 1, 4, 5 ) == 5.0
    """
    return sqrt( (sor2-sor1)**2 + (oszlop2-oszlop1)**2)

hajo_sor    = random_sor(jatekmezo)
hajo_oszlop = random_oszlop(jatekmezo)

# Játék kezdete

for loves in range(1,LOVESEK+1):
    print_jatekmezo(jatekmezo)
    print ("%d. lövésed következik (összesen van %d lőszered)" % (loves, LOVESEK))

    while True:
        try:
            tipp_sor    = int(input("Sor    (1.." + str(SOROK   ) + "): ") ) -1
            break
        except ValueError:
            # kis/nagybetű lényeges, próbáld ki
            print( "Ez nem egész szám, próbáld újra..." )
        except KeyboardInterrupt:
            print ("Nem ér menekülni...")

    # oszlop-ra hasonlóan...
    tipp_oszlop = int(input("Oszlop (1.." + str(OSZLOPOK) + "): ") ) -1

    if ((tipp_sor == hajo_sor) and (tipp_oszlop == hajo_oszlop)):
        print ("Talált, süllyedt! Győztél!")
        break
    else:
        if (tipp_sor < 0 or tipp_sor >= SOROK) or (tipp_oszlop < 0 or tipp_oszlop >= OSZLOPOK):
            print ("Ez nincs is a táblán.")
        elif (jatekmezo[tipp_sor][tipp_oszlop] == "X"):
            print ("Oda már lőttél!" )
        else:
            print ( "Nem talált!" )
            print ( "Távolság: " + str ( tavolsag( tipp_sor, tipp_oszlop, hajo_sor, hajo_oszlop ) ) )
            jatekmezo[tipp_sor][tipp_oszlop] = "X"

        if (loves >= LOVESEK):
            print ("Nincs több lőszered!") 
            break

        # TODO: Esc-et elkapni
        # vagy: beleírni a szövegbe, hogy Esc-pel kiléphet itt most.
        input("Üss Enter-t a folytatáshoz!")

print ("Vége a játéknak!")

