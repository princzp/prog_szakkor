"""
    Kiszámolja egy szám abszolút értékét.
    Az elágazás fogalmának bevezetése.
    Átvezetés a kivételkezeléshez.
"""
szam_str = input ("Írj be egy számot, megmondom az abszolút értékét! ")
szam = float(szam_str)

##if szam < 0 :
##    szam_abs = -szam
##else:
##    if szam == 0:
##        szam_abs = szam
##    else:
##        if szam > 0:
##            szam_abs = szam
##        else:
##            # mi van, ha nem is szám?
##            pass

if szam < 0:
    szam_abs = -szam
elif szam == 0:
    szam_abs = szam
elif szam > 0:
    szam_abs = szam
else:
    # mi van, ha nem is szám?
    # Sajnos nem ilyen egyszerű, nem ide fut be
    # Hanem ValueError kivételt dob...
    pass

print ("Az abszolút értéke: " , szam_abs)
